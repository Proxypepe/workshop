
Actual version 3.27.2
Last release - 3.27.2 - last month

calico/kube-controllers:3.27.2
calico/node:3.27.2
calico/cni:3.27.2


Issue to [Calico v3.28.0](https://github.com/projectcalico/calico/milestone/236)
**75%** complete

https://github.com/projectcalico/node -> https://github.com/projectcalico/calico

## tag 
### Opened
Fix 5.9 CVE-2024-24786
Opened 2 issue
[Bump google.golang.org/protobuf from 1.31.0 to 1.33.0](https://github.com/projectcalico/calico/pull/8611)
[bump iptables version to v1.8.8](https://github.com/projectcalico/calico/pull/8416)

### Closed

https://github.com/projectcalico/calico/pull/8128 ->[**Calico v3.27.0**](https://github.com/projectcalico/calico/milestone/228 "Calico v3.27.0")

[Bump github.com/opencontainers/runc from 1.1.6 to 1.1.12](https://github.com/projectcalico/calico/pull/8468)

[Bump github.com/containerd/containerd from 1.6.23 to 1.6.26](https://github.com/projectcalico/calico/pull/8355)

[Bump github.com/cyphar/filepath-securejoin from 0.2.3 to 0.2.4](https://github.com/projectcalico/calico/pull/7999)




calico/node:v3.29.0-0.dev-2-g57cdffe55d35 (redhat 8.9)


![[Pasted image 20240318194729.png]]

![[Pasted image 20240318194746.png]]

![[Pasted image 20240318200927.png]]

Используются пакеты 2019-2023

| library         | CVE            | Installed Version             | Fixed by CVE                                                    | link                                       |
| --------------- | -------------- | ----------------------------- | --------------------------------------------------------------- | ------------------------------------------ |
| bzip2-libs      | CVE-2019-12900 | 1.0.6-26.el8                  | <br>1.0.6                                                       | https://avd.aquasec.com/nvd/cve-2019-12900 |
| ca-certificates | CVE-2023-37920 | 2023.2.60_v7.0.306-80.0.el8_8 | * (2023.07.22)                                                  | https://avd.aquasec.com/nvd/cve-2023-37920 |
| elfutils-libelf | CVE-2021-33294 | 0.189-3.el8                   | 0.183                                                           | https://avd.aquasec.com/nvd/cve-2021-33294 |
| elfutils-libelf | CVE-2024-25260 | 0.189-3.el8                   | opened issue https://github.com/schsiung/fuzzer_issues/issues/1 | https://avd.aquasec.com/nvd/cve-2024-25260 |
| libgcc          | CVE-2021-42694 | 8.5.0-20.el8                  | *                                                               | https://avd.aquasec.com/nvd/cve-2021-42694 |
| libgcc          | CVE-2018-20657 | 8.5.0-20.el8                  | *                                                               | https://avd.aquasec.com/nvd/cve-2018-20657 |
| libgcc          | CVE-2019-14250 | 8.5.0-20.el8                  | *                                                               | https://avd.aquasec.com/nvd/cve-2019-14250 |
| libgcc          | CVE-2022-27943 | 8.5.0-20.el8                  | *                                                               | https://avd.aquasec.com/nvd/cve-2022-27943 |
| libzstd         | CVE-2021-24032 | 1.4.4-1.el8                   | *                                                               | https://avd.aquasec.com/nvd/cve-2021-24032 |
| libzstd         | CVE-2022-4899  | 1.4.4-1.el8                   | found in 1.4.10                                                 | https://avd.aquasec.com/nvd/cve-2022-4899  |
| ncurses-libs    | CVE-2018-19211 | 6.1-10.20180224.el8           | 6.1                                                             | https://avd.aquasec.com/nvd/cve-2018-19211 |
| ncurses-libs    | CVE-2018-19217 | 6.1-10.20180224.el8           | *                                                               | https://avd.aquasec.com/nvd/cve-2018-19217 |
| ncurses-libs    | CVE-2020-19185 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19185 |
| ncurses-libs    | CVE-2020-19186 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19186 |
| ncurses-libs    | CVE-2020-19187 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19187 |
| ncurses-libs    | CVE-2020-19188 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19188 |
| ncurses-libs    | CVE-2020-19189 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19189 |
| ncurses-libs    | CVE-2020-19190 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19190 |
| ncurses-libs    | CVE-2021-39537 | 6.1-10.20180224.el8           | s 6.2.1 e 6.2.1                                                 | https://avd.aquasec.com/nvd/cve-2021-39537 |
| ncurses-libs    | CVE-2023-50495 | 6.1-10.20180224.el8           | s 6.4-20230418 e 6.4-20230418                                   | https://avd.aquasec.com/nvd/cve-2023-50495 |
| ncurses-libs    | CVE-2018-19211 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2018-19211 |
| ncurses-libs    | CVE-2018-19217 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2018-19217 |
| ncurses-libs    | CVE-2020-19185 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19185 |
| ncurses-libs    | CVE-2020-19186 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19186 |
| ncurses-libs    | CVE-2020-19187 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19187 |
| ncurses-libs    | CVE-2020-19188 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19188 |
| ncurses-libs    | CVE-2020-19189 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19189 |
| ncurses-libs    | CVE-2020-19190 | 6.1-10.20180224.el8           | 6.1 *                                                           | https://avd.aquasec.com/nvd/cve-2020-19190 |
| ncurses-libs    | CVE-2021-39537 | 6.1-10.20180224.el8           | 6.2.1                                                           | https://avd.aquasec.com/nvd/cve-2021-39537 |
| ncurses-libs    | CVE-2023-50495 | 6.1-10.20180224.el8           | s 6.4-20230418 e 6.4-20230418                                   | https://avd.aquasec.com/nvd/cve-2023-50495 |
| openssl-libs    | CVE-2023-0464  | 1:1.1.1k-12.el8_9             | ? Есть патч                                                     | https://avd.aquasec.com/nvd/cve-2023-0464  |
| openssl-libs    | CVE-2023-0465  | 1:1.1.1k-12.el8_9             | ? Есть патч                                                     | https://avd.aquasec.com/nvd/cve-2023-0465  |
| openssl-libs    | CVE-2023-0466  | 1:1.1.1k-12.el8_9             | ? Есть патч                                                     | https://avd.aquasec.com/nvd/cve-2023-0466  |
| openssl-libs    | CVE-2023-2650  | 1:1.1.1k-12.el8_9             | ? Есть патч                                                     | https://avd.aquasec.com/nvd/cve-2023-2650  |
| openssl-libs    | CVE-2024-0727  | 1:1.1.1k-12.el8_9             | ? Есть патч                                                     | https://avd.aquasec.com/nvd/cve-2024-0727  |
| pcre2           | CVE-2022-41409 | 10.32-3.el8_6                 | ? Есть патч 2-10.43                                             | https://avd.aquasec.com/nvd/cve-2022-41409 |
| systemd-libs    | CVE-2018-20839 | 239-78.el8                    | s  242 e 242 Есть патч                                          | https://avd.aquasec.com/nvd/cve-2018-20839 |
| systemd-libs    | CVE-2023-7008  | 239-78.el8                    | *                                                               | https://avd.aquasec.com/nvd/cve-2023-7008  |
| systemd-libs    | CVE-2021-3997  | 239-78.el8                    | * Есть патч                                                     | https://avd.aquasec.com/nvd/cve-2021-3997  |

![[Pasted image 20240319123212.png]]


![[Pasted image 20240319123517.png]]

https://linuxsoft.cern.ch/cern/centos/s9/BaseOS/x86_64/os/Packages/
https://linuxsoft.cern.ch/centos-vault/8.4.2105/BaseOS/x86_64/os/Packages/
https://linuxsoft.cern.ch/centos-vault/8.4.2105/AppStream/x86_64/os/Packages/



https://quay.io/repository/centos/centos/manifest/sha256:6b7c6df25b713181a656c340e91450068125cb2596d4c10e7a634eacc7268bee?tab=packages

