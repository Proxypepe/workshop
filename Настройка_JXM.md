

## Настройка http server конфигурации
```
1. https://jfrog.com/help/r/how-to-enable-jmx-monitoring-in-artifactory-7-x/how-to-enable-jmx-monitoring-in-artifactory-7.x
2. https://jfrog.com/help/r/how-to-use-jmx-remote-monitoring/how-to-use-jmx-remote-monitoring
3. https://jfrog.com/help/r/jfrog-platform-administration-documentation/artifactory-jmx-mbeans
```
1. Добавить Java параметры

$JFROG_HOME/artifactory/var/etc/system.yaml

```yaml
shared:  
    extraJavaOpts: "-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9010 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=<YOUR_ARTIFACTORY_SERVER_HOSTNAME_OR_IP>"
```

2. Перезагрузить Artifactory
3. Скачать jmx exporter
```bash
wget https://repo.maven.apache.org/maven2/io/prometheus/jmx/jmx_prometheus_httpserver/0.20.0/jmx_prometheus_httpserver-0.20.0.jar
```
4. Сконфигурировать jmx и поднять Exporter
config.yaml
```yaml
hostPort: <YOUR_ARTIFACTORY_SERVER_HOSTNAME_OR_IP>:9010
rules:
- pattern: ".*"
```

```bash
java -jar jmx_prometheus_httpserver-0.20.0.jar <EXPORTER_PORT> config.yaml
```

```bash
nohup java -jar jmx_prometheus_httpserver-0.20.0.jar 12345 config.yaml &
```

5. Настроить Prometheus 

```yaml
scrape_configs:
  - job_name: "jmx"

    static_configs:
      - targets: ["<EXPORTER_SERVER_HOSTNAME_OR_IP>:<EXPORTER_PORT>"]

```


![[Pasted image 20240325141150.png]]

![[Pasted image 20240325141215.png]]

## Настройка java agent конфигурации


1. Скачать jmx exporter

```bash
wget https://repo.maven.apache.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.20.0/jmx_prometheus_javaagent-0.20.0.jar
```

2. Произвести конфигурирование jmx exporter
config.yaml
```yaml
rules:
- pattern: ".*"
```


3. Разместить jar и конфигурацию в директории

```bash
mv jmx_prometheus_javaagent-0.20.0.jar /etc/jmx_exporter/jmx_agent.jar
```

4. Добавить Java параметры

$JFROG_HOME/artifactory/var/etc/system.yaml

```yaml
shared:  
    extraJavaOpts: "-javaagent:/etc/jmx_exporter/jmx_agent.jar=12345:/etc/jmx_exporter/config.yaml"
```

5. Перезагрузить Artifactory